"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const vscode_languageclient_1 = require("vscode-languageclient");
let client;
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    let serverOptions = {
        command: 'why3_lsp_server',
        args: ['']
    };
    let clientOptions = {
        documentSelector: [{ scheme: 'file', language: 'whyml' }],
    };
    const restart = () => {
        if (client) {
            client.stop();
        }
        client = new vscode_languageclient_1.LanguageClient('Why3-LSP-Server', 'Why3 Language Server', serverOptions, clientOptions);
        //client.onReady().then(() => {
        //});
        context.subscriptions.push(client.start());
    };
    vscode.commands.registerCommand('extension.why3-lsp.prove', prove);
    restart();
}
exports.activate = activate;
function prove() {
    var _a;
    client.sendRequest("workspace/executeCommand", { command: "prove", arguments: { file: (_a = vscode.window.activeTextEditor) === null || _a === void 0 ? void 0 : _a.document.uri.path } });
}
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map