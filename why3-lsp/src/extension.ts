// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import {
    LanguageClient,
    LanguageClientOptions,
    ServerOptions,
    TransportKind,
    RequestType,
    NotificationType,
    TextDocumentIdentifier,
    RegistrationRequest,
} from 'vscode-languageclient';

let client: LanguageClient;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	let serverOptions = {
        command: 'why3_lsp_server',    // XXX: Get from configuration
        args: [ '' ]
    };

    let clientOptions: LanguageClientOptions = {
        documentSelector: [{ scheme: 'file', language: 'whyml' }],
	};

	const restart = () => {

        if (client) {
            client.stop();
        }

        client = new LanguageClient(
            'Why3-LSP-Server',
            'Why3 Language Server',
            serverOptions,
            clientOptions
        );

        //client.onReady().then(() => {
        //});
		context.subscriptions.push(client.start());
    };

    vscode.commands.registerCommand('extension.why3-lsp.prove', prove);

    restart();
}

function prove() {
    if(client !== undefined) {
        client.sendRequest("workspace/executeCommand", {command : "prove", arguments : {file : vscode.window.activeTextEditor?.document.uri.path}});
    }
}

// this method is called when your extension is deactivated
export function deactivate() {}
