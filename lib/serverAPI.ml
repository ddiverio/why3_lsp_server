open Idl

module ServerAPI(R : RPC) = struct
  open R

  let description = Interface.{
      name = "Why3 LSP Server";
      namespace = None;
      description = [ "A LSP server for WhyML" ];
      version=(0,0,1);
    }
  let implementation = implement description

  (* Should be changed *)
  let err = Datatypes.err
  let unit_p = Param.mk Rpc.Types.unit

  (* Initialize request *)
  let init_rp = Param.mk
      Datatypes.Initialize.initializeParams
  let init_rep = Param.mk
      Datatypes.Initialize.initializeResult

  (* Initialization/shutdown requests and notifications *)
  let initialize = declare
      "initialize"
      ["Initializes the server"]
      (init_rp @-> returning init_rep err)
  let initialized_n = declare_notification
      "initialized"
      []
      (returning unit_p err)
  let shutdown = declare
      "shutdown"
      ["Shutdowns the server"]
      (returning unit_p err)
  let exit_n = declare_notification
      "exit"
      []
      (unit_p @-> returning unit_p err)
  
  (* Opening/closing documents *)
  let didOpenTextDocument_p = Param.mk
      Datatypes.didOpenTextDocumentParams
  
  let didOpen = declare_notification
      "textDocument/didOpen"
      ["Opens a document"]
      (didOpenTextDocument_p @-> returning unit_p err) 

  let didCloseTextDocument_p = Param.mk
      Datatypes.didCloseTextDocumentParams

  let didClose = declare_notification
      "textDocument/didClose"
      ["Closes a document"]
      (didCloseTextDocument_p @-> returning unit_p err)

  let didSaveTextDocument_p = Param.mk
      Datatypes.didSaveTextDocumentParams
  let didSave = declare_notification
      "textDocument/didSave"
      [""]
      (didSaveTextDocument_p @-> returning unit_p err)

  (* Addressing configuration changes *)
  let didChangeConfiguration_p = Param.mk
      Datatypes.didChangeConfigurationParams
  
  let didChangeConfiguration = declare_notification
      "workspace/didChangeConfiguration"
      [""]
      (didChangeConfiguration_p @-> returning unit_p err)

  (* goto definition/declaration *)
  let declaration_p = Param.mk
      Datatypes.textDocumentPositionParams
  let declaration_r = Param.mk
      Datatypes.location
  let declaration = declare
    "textDocument/declaration"
    [""]
    (declaration_p @-> returning declaration_r err)

  (* Executing commands *)
  let executeCommand_p = Param.mk
      Datatypes.executeCommandParams
  let executeCommand_r = Param.mk
      Datatypes.commandResponse

  let executeCommand = declare
      "workspace/executeCommand"
      [""]
      (executeCommand_p @-> returning executeCommand_r err)
end
