open Idl

module ClientAPI(R : RPC) = struct
  open R

  let description = Interface.{
      name = "Why3 LSP Client interface";
      namespace = None;
      description = ["An implementation of the LSP interface client-side"];
      version=(0,0,1);
    }
  let implementation = implement description

  let unit_p = Param.mk Rpc.Types.unit
  let err = Datatypes.err

  let sm_p = Param.mk
      Datatypes.showMessageParams

  let showMessage = declare_notification
    "window/showMessage"
    [""]
    (sm_p @-> returning unit_p err)

  let logMessage = declare_notification
    "window/logMessage"
    [""]
    (sm_p @-> returning unit_p err)

  let pd_p = Param.mk
      Datatypes.publishDiagnosticsParams

  let publishDiagnostics = declare_notification
    "textDocument/publishDiagnostics"
    [""]
    (pd_p @-> returning unit_p err)
end
