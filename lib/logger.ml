open Unix

let logfile = Filename.concat (Unix.getenv "HOME") ".why3lsplog"

let string_date () =
  let td = localtime (time ()) in
  Format.sprintf "%d:%d:%d, %d/%d/%d" td.tm_hour td.tm_min td.tm_sec 
    (td.tm_wday+1) (td.tm_mon+1) (td.tm_year + 1900)

let log_to_file (str : string) =
  let oc = open_out_gen [Open_append; Open_creat] 0o666 logfile in
  Printf.fprintf oc "%s\n" str;
  close_out oc
