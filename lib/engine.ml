open Datatypes
open Transport
open Util

module M=Idl.IdM
module MyIdl=Idl.Make(M)
module Client=ClientAPI.ClientAPI(MyIdl.GenClient())

open MyIdl

let openFiles : (string, string) Hashtbl.t = Hashtbl.create 5

let getFileContent = Hashtbl.find openFiles

(* Initializes the LSP server *)
let initialize init_rp =
  let open Initialize in
  let result =
    (* to be moved elsewhere *)
    { capabilities =
        Some {
          hoverProvider = Some false;
          definitionProvider = Some false;
          declarationProvider = Some true;
          executeCommandProvider = Some { commands = ["prove"] };
          textDocumentSync = Some {
            openClose = Some true;
            change = None;
            willSave = None;
            willSaveWaitUntil = None;
            save = Some { includeText = Some true; }}
        };
    } in
  let workspacePath = init_rp.rootPath in
  Parser.initializeEnvironment workspacePath;
  ignore init_rp; (* tbd *)
  ErrM.return result

let shutdown () = (* tbd *)
  ErrM.return ()

let exit_n () = (* tbd *)
  exit 0

let didOpen (didO_p : didOpenTextDocumentParams) =
  Hashtbl.add openFiles (uri_to_path didO_p.textDocument.uri) didO_p.textDocument.text;
  ErrM.return ()

let didClose (didC_p : didCloseTextDocumentParams) =
  Hashtbl.remove openFiles (uri_to_path didC_p.textDocument.uri);
  ErrM.return ()

let didSave (didS_p : didSaveTextDocumentParams) =
  let file = uri_to_path didS_p.textDocument.uri in
  match didS_p.text with
  | Some text -> Hashtbl.replace openFiles file text; ErrM.return ()
  | None -> ErrM.return_err (errInternal "Text has not been sent by LSP client, it should not happen.")

let didChangeConfiguration didCC_p = (* tbd *)
  ignore didCC_p;
  ErrM.return ()

let proveFile execCmd_p =
  match execCmd_p.arguments with
  | Some r -> begin match r.file with
      | Some filename ->
        let diag_p = Parser.parseFile filename in
        let (>>=)  = ErrM.bind in
        Client.publishDiagnostics call_client diag_p
        >>=
        (fun _ -> ErrM.return { response = "why3 prove execution succeeded" })
      | None -> ErrM.return_err (errInternal "No filename given: prove failed.") end
  | None -> ErrM.return_err (errInternal "No command arguments given at all: prove failed.")

let replaySession execCmd_p =
  ignore execCmd_p; ErrM.return_err (errInternal "")

let hashtblToList h =
  let f _ x l = x::l in
  Why3.Session_itp.Hfile.fold f h []

(* logs to stderr atm *)
(* TODO: create better status messages *)
let callback status =
  let open Why3.Controller_itp in
  match status with
  | STShalt -> Format.eprintf "halt@."
  | STSgoto (_, _) -> Format.eprintf "goto@."
  | STSfatal (_, _, _) -> Format.eprintf "fatal@."

let diagnostic_of_proofAttemptId session paId message =
  let pId = Why3.Session_itp.get_proof_attempt_parent session paId in
  let ident = Why3.Session_itp.get_proof_name session pId in
  let (_, l, c1, c2) = begin match ident.id_loc with None -> ("", 0, 0, 0) | Some c -> Why3.Loc.get c end in
  let diag_p = { Util.basicDiag with range = Util.build_range l c1 c2;
                                     message = message;                                     severity = Some 1 }
  in diag_p


let callback_pa refDiagList session = fun paId paStatus ->
  let open Why3.Controller_itp in
  match paStatus with
  | Done result ->
    (* publish info about result *)
    begin match result.pr_answer with
    | Valid -> Format.eprintf "valid\n"
    | Invalid | Timeout | OutOfMemory | StepLimitExceeded | HighFailure (* failed *) ->
      let diag_p = diagnostic_of_proofAttemptId session paId "failure" in
      refDiagList := diag_p::!refDiagList
    | Unknown m ->
      let diag_p = diagnostic_of_proofAttemptId session paId m in
      refDiagList := diag_p::!refDiagList
    | Failure m -> Format.eprintf "%s\n" m
    end
  | Undone -> Format.eprintf "undone\n"
  | Scheduled -> Format.eprintf "sched\n"
  | Running -> Format.eprintf "running\n"
  | Interrupted -> Format.eprintf "int\n"
  | Detached -> Format.eprintf "detach\n"
  | InternalFailure e -> Format.eprintf "intfail %s\n" (Printexc.to_string e)
  | Uninstalled _ -> Format.eprintf "not installed\n"
  | UpgradeProver _ -> Format.eprintf "upgrade\n"
  | Removed _ -> Format.eprintf "removed\n"

let createController filename =
  let sessdir = Filename.remove_extension filename in
  let (env, config) = Parser.createEnvironment filename in
  let (session, _) = Why3.Session_itp.load_session sessdir in (* add exn handling *)
  let controller = Why3.Controller_itp.create_controller config env session in
  Why3.Server_utils.load_strategies controller; (controller, session)

let goalsFromSession session =
  let files = hashtblToList (Why3.Session_itp.get_files session) in
  let theories = List.concat (List.map Why3.Session_itp.file_theories files) in
  let goals = List.concat (List.map Why3.Session_itp.theory_goals theories) in
  goals

let autolevel1 execCmd_p =
  match execCmd_p.arguments with
  | Some r -> begin match r.file with
      | Some filename ->
        (* TODO: do not use Unix_scheduler, which doesn't work for this use *)
        let module C = Why3.Controller_itp.Make(Scheduler.LSP_Scheduler) in
        let (controller,session) = createController filename in
        let goals = goalsFromSession session in
        let (_,_,_,st) = Why3.Wstdlib.Hstr.find controller.controller_strategies "Auto_level_1" in
        (* Diagnostic publishing *)
        let callback_tr _ _ _ = Format.eprintf "tr\n" (* tmp *) in
        let notification _ = () (* to be left as it is. Maybe not. Idk. *) in
        let diagList = ref [] in
        let callback_pa = callback_pa diagList session in
        let run_strat n = C.run_strategy_on_goal controller n st ~callback_pa ~callback_tr ~callback ~notification in
        List.iter run_strat goals;
        Scheduler.LSP_Scheduler.main_loop ();
        Format.eprintf "%d\n@." (List.length !diagList);
        let diag_p = { uri = filename; diagnostics = !diagList } in
        let (>>=)  = ErrM.bind in
        Client.publishDiagnostics call_client diag_p
        >>=
        (fun _ -> ErrM.return { response = "why3 autolevel execution succeeded" })
      | None -> ErrM.return_err (errInternal "No filename given: autolevel1 failed.") end
  | None -> ErrM.return_err (errInternal "No command arguments given at all: autolevel1 failed.")

let commands = [("prove", proveFile) ; ("replay", replaySession) ; ("autolevel1", autolevel1)]

let executeCommand execCmd_p =
  let cmd = execCmd_p.command in
  match List.assoc_opt cmd commands with
  | Some call -> call execCmd_p
  | None -> ErrM.return_err (errMethodNotFound "Command not implemented.")

let declaration textDocPos_p =
  let uri = textDocPos_p.textDocument.uri in
  let file = uri_to_path uri in
  let buf = getFileContent file in
  let (l,c) = (textDocPos_p.position.line, textDocPos_p.position.character) in
  let offset = find_ident_start buf (pos_to_buffer_pos buf l c) in
  let (l,c) = (l+1, c+offset) in
  Format.eprintf "offset : %d@." offset;
  Format.eprintf "fichier : %s@." file;
  let pos = Why3.Loc.user_position file l c c in
  (* we don't care about the syntax errors here, we parse up to the error in order
     to have information at least up to this point. *)
  (try Parser.basicParse file with _ -> ());
  try
    let decl_ident,_,_ = Why3.Glob.find pos in
    let pos = (match decl_ident.id_loc with
        | None -> raise Not_found
        | Some pos -> pos)
    in
    ErrM.return (pos_to_location pos)
  with Not_found -> ErrM.return_err (errInternal "Failed to retrieve identifier")
