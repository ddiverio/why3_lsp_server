let process_header () =
  (* consume empty lines *)
  let rec consume_empty () =
    let x = read_line () in
    if String.length x == 0 then consume_empty () else x
  in
  let rec header () =
    let x = read_line () in
    if String.length (String.trim x) == 0 then []
    else x :: header ()
  in
  let fst_line = consume_empty () in
  let header = fst_line :: header () in
  header

let parse_header header =
  let parse_line l =
    match l with
    | (h::c::_) ->
      if String.equal h "Content-Length" then
        int_of_string (String.trim c)
      else -1
    | _ -> 0
  in
  let results = List.map parse_line (List.map (String.split_on_char ':') header)
  in
  List.fold_left max (-1) results

let make_header length =
  Format.sprintf "Content-Length: %d\r\n\r\n" length

let make_rpc_message msg =
  String.concat "" [make_header (String.length msg); msg]
