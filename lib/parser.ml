open Util
open Datatypes

(* pairs (filename, (env, config)) *)
let why3data = Hashtbl.create 10

let initializeEnvironment workspace =
  ignore workspace

let createEnvironment filename =
  let curdir = Filename.dirname filename in
  let config_file = Filename.concat curdir ".why3_lsp_server.conf" in
  let config_file = if Sys.file_exists config_file then [config_file] else [] in
  let config = Why3.Whyconf.read_config None in
  let config = List.fold_left Why3.Whyconf.merge_config config config_file in
  let config = !Why3.Whyconf.provers_from_detected_provers config in
  let main = Why3.Whyconf.get_main config in
  let loadpath = Why3.Whyconf.loadpath main in
  Format.eprintf "Loadpath: %s@." (String.concat ";" loadpath);
  let env = Why3.Env.create_env loadpath in
  Hashtbl.add why3data filename (env, config);
  (env, config)

let lookupWhyData filename =
  let data = Hashtbl.find_opt why3data filename in
  match data with
  | Some d -> d
  | None   -> createEnvironment filename

let lookupEnvironment filename =
  let (e,_) = lookupWhyData filename in e

let lookupConfig filename =
  let (_,c) = lookupWhyData filename in c

let basicParse file =
  let env = lookupEnvironment file in
  ignore (Why3.Env.read_file Why3.Env.base_language env file)

let parseFile file =
  let basicDiagParams = {
    uri = file; diagnostics = []
  } in
  try
    ignore (basicParse file);
    basicDiagParams
  with
  | Why3.Loc.Located(loc, e) ->
    let (f, l, c1, c2) = Why3.Loc.get loc in
    let msg = Format.asprintf "%a: %a" Why3.Loc.gen_report_position loc Why3.Exn_printer.exn_printer e in
    let diag = { basicDiag with range = build_range l c1 c2;
                                message = msg;
                                severity = Some 1 (* error *)}
    in
    { uri = f ; diagnostics = [diag] }
  | e ->
    Logger.log_to_file (Printexc.to_string e);
    basicDiagParams
