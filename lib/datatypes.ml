open Idl

(* All specifications taken from 
   <https://microsoft.github.io/language-server-protocol/specifications/specification-3-14/>

   Comments in types are yet to be implemented fields from the specification.
*)

module Initialize = struct
  type workspaceEdit = {
    documentChanges : bool option;
    (* resourceOperations?: ResourceOperationKind[]; *)
    (* failureHandling?: FailureHandlingKind; *)
  } [@@deriving rpcty]
  type dynRegistration = {
    dynamicRegistration : bool option;
  } [@@deriving rpcty]
  (* symbolKind?: {
     /**
   * The symbol kind values the client supports. When this
   * property exists the client also guarantees that it will
     * handle values outside its set gracefully and falls back
   * to a default value when unknown.
   *
   * If this property is not present the client only supports
   * the symbol kinds from `File` to `Array` as defined in
     * the initial version of the protocol.
   */
     valueSet?: SymbolKind[];
     }
     } *)
  (* to be implemented accurately *)
  type symbolKind = unit [@@deriving rpcty]
  type symbol = {
    dynamicRegistration : bool option;
    symbol : symbolKind option; (* tbd, i.e symbolKind comment *)
  } [@@deriving rpcty]
  type workspaceClientCapabilities = {
    applyEdit : bool option (*[@default true] *) ;
    workspaceEdit : workspaceEdit;
    didChangeConfiguration : dynRegistration option;
    didChangeWatchedFiles : dynRegistration option;
    executeCommand : dynRegistration option;
    workspaceFolders : bool option;
    configuration : bool option;
  } [@@deriving rpcty]
  type synchronization = {
    dynamicRegistration : bool option;
    willSave : bool option;
    willSaveWaitUntil : bool option;
    didSave : bool option;
  } [@@deriving rpcty]
  module TextDocumentClientCapabilities = struct
    module Completion = struct
      type t = {
        dynamicRegistration : bool option;
        completionItem : completionItem option;
        contextSupport : bool option;
        (* yet to be implemented, but all types are options, so we'll see later *)
      } [@@deriving rpcty]
      and completionItem = {
        snippetSupport : bool option;
        commitCharactersSupport : bool option;
        (* documentationFormat?: MarkupKind[]; *)
        deprecatedSupport : bool option;
        (* completionItemKind?: {
                     /**
         * The completion item kind values the client supports. When this
         * property exists the client also guarantees that it will
         * handle values outside its set gracefully and falls back
         * to a default value when unknown.
         *
         * If this property is not present the client only supports
         * the completion items kinds from `Text` to `Reference` as defined in
         * the initial version of the protocol.
         */
                     valueSet?: CompletionItemKind[];
                     },
        *)
        preselectSupport : bool option;
      } [@@deriving rpcty]
    end
    type hover = {
      dynamicRegistration : bool option;
      (* contentFormat?: MarkupKind[]; *)
    } [@@deriving rpcty]
    type t = {
      synchronization : synchronization option;
      completion : Completion.t;
      hover : hover option;
      (* yet to be implemented, but all types are options, so we'll see later *)
    } [@@deriving rpcty]
  end

  type clientCapabilities = {
    workspace : workspaceClientCapabilities option;
    textDocument : TextDocumentClientCapabilities.t option;
    (* experimental?: any; *)
  } [@@deriving rpcty]
  type initializeParams = {
    processId : int;
    rootPath : string option;
    (* 	initializationOptions?: any; *)
    rootUri : string option;
    (* trace?: 'off' | 'messages' | 'verbose'; *)
    trace : string option;
    capabilities : clientCapabilities;
    (* workspaceFolders?: WorkspaceFolder[] | null; *)
  } [@@deriving rpcty]
  type executeCommandOptions = {
    commands : string list;
  } [@@deriving rpcty]
  type saveOptions = {
    includeText : bool option;
  } [@@deriving rpcty]
  type textDocumentSyncOptions = {
    openClose : bool option;
    change : bool option;
    willSave : bool option;
    willSaveWaitUntil : bool option;
    save : saveOptions option;
  } [@@deriving rpcty]
  type serverCapabilities = {
    (* textDocumentSync?: TextDocumentSyncOptions | number; *)
    hoverProvider : bool option;
    (* completionProvider?: CompletionOptions; *)
    (* signatureHelpProvider?: SignatureHelpOptions; *)
    executeCommandProvider : executeCommandOptions option;
    definitionProvider : bool option;
    declarationProvider : bool option;
    textDocumentSync : textDocumentSyncOptions option;
  } [@@deriving rpcty]
  type initializeResult = {
    capabilities : serverCapabilities option;
  } [@@deriving rpcty]
end
type documentUri = string [@@deriving rpcty]
type textDocumentIdentifier = {
  uri : documentUri;
} [@@deriving rpcty]
type position = {
  line : int;
  character : int;
} [@@deriving rpcty]
type range = {
  start : position;
  end_ : position [@key "end"];
} [@@deriving rpcty]
type location = {
  uri : documentUri;
  range : range;
} [@@deriving rpcty]
type locationLink = {
  originSelectionRange : range option;
  targetUri : documentUri;
  targetRange : range;
  targetSelectionRange : range;
} [@@deriving rpcty]
type diagnosticRelatedInformation = {
  location : location;
  message : string;
} [@@deriving rpcty]
type diagnostic = {
  range : range;
  severity : int option;
  code: string option;
  source : string option;
  message : string;
  relatedInformation : diagnosticRelatedInformation list option;
} [@@deriving rpcty]
type command = {
  title : string;
  command : string;
  (* arguments?: any[]; *)
} [@@deriving rpcty]
type textEdit = {
  range : range;
  newText : string;
} [@@deriving rpcty]
type textDocumentItem = {
  uri : documentUri;
  languageId : string;
  version : int;
  text : string;
} [@@deriving rpcty]
type didOpenTextDocumentParams = {
  textDocument : textDocumentItem;
} [@@deriving rpcty]
type didCloseTextDocumentParams = {
  textDocument : textDocumentIdentifier;
} [@@deriving rpcty]
type didChangeConfigurationParams = {
  settings : string list;
} [@@deriving rpcty]
type didSaveTextDocumentParams = {
  textDocument : textDocumentIdentifier;
  text : string option;
} [@@deriving rpcty]
(* executeCommand *)
type commandArgs = { file: string option } [@@deriving rpcty]
type commandResponse = { response : string } [@@deriving rpcty]
type executeCommandParams = {
  command : string;
  arguments : commandArgs option;
} [@@deriving rpcty]

(* workspace/applyEdit *)
type workspaceEdit = {
  changes : string;
} [@@deriving rpcty]
type applyWorkspaceEditParams = {
  label : string option;
  edit : workspaceEdit;
} [@@deriving rpcty]

(* window/showMessage *)
type showMessageParams = {
  type_ : int; [@key "type"]
  message : string;
} [@@deriving rpcty]

(* window/logMessage *)
type logMessageParams = showMessageParams [@@deriving rpcty]
(* To be taken care of:

   export namespace MessageType {
   /**
 * An error message.
 */
   export const Error = 1;
   /**
 * A warning message.
 */
   export const Warning = 2;
   /**
 * An information message.
 */
   export const Info = 3;
   /**
 * A log message.
 */
   export const Log = 4;
   }

*)
(* textDocument/publishDiagnostics *)
type publishDiagnosticsParams = {
  uri : documentUri;
  diagnostics : diagnostic list 
} [@@deriving rpcty]

(* textDocument/declaration *)
type textDocumentPositionParams = {
  textDocument : textDocumentIdentifier;
  position : position;
} [@@deriving rpcty]

(** The error type for JSON-RPC/LSP *)
type responseError = {
  code : int;
  message : string;
  data : string
} [@@deriving rpcty]

exception DatatypeError of responseError

let err = Error.{
    def = responseError;
    raiser = (function | e -> raise (DatatypeError e));
    matcher = function | DatatypeError e -> Some e | _ -> None
  }
let createError code msg = {
  code = code;
  message = msg;
  data = ""
}
let errMethodNotFound = createError (-32601)
let errInternal =       createError (-32603)
(* export namespace ErrorCodes {
   // Defined by JSON RPC
   export const ParseError: number = -32700;
   export const InvalidRequest: number = -32600;
   export const MethodNotFound: number = -32601;
   export const InvalidParams: number = -32602;
   export const InternalError: number = -32603;
   export const serverErrorStart: number = -32099;
   export const serverErrorEnd: number = -32000;
   export const ServerNotInitialized: number = -32002;
   export const UnknownErrorCode: number = -32001;

   // Defined by the protocol.
   export const RequestCancelled: number = -32800;
   export const ContentModified: number = -32801;
   } *)
