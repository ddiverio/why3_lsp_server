open Logger

module M=Idl.IdM
module MyIdl=Idl.Make(M)

let serve_next_request process =
  let len = Headers.parse_header (Headers.process_header ()) in
  let content = really_input_string Stdlib.stdin len in
  log_to_file content;
  let open M in
  process content >>= 
  fun result ->
  (if not result.Rpc.notif then
     let (_, id', _) = Jsonrpc.version_id_and_call_of_string_option content in
     let id = match id' with Some i -> i | None -> Rpc.rpc_of_int 0 in
     let rep_s = Jsonrpc.string_of_response ~version:V2 ~id:id result in
     let rep = Headers.make_rpc_message rep_s in
     log_to_file rep;
     Format.printf "%s" rep;
     M.return ()
   else
     begin
       M.return ()
     end)

let call_client call =
  let call_msg = Jsonrpc.string_of_call ~version:V2 call in
  let msg = Headers.make_rpc_message call_msg in
  Logger.log_to_file msg;
  Printf.printf "%s" msg;
  M.return (if not call.notif then
              let len = Headers.parse_header (Headers.process_header ()) in
              let content = really_input_string Stdlib.stdin len in
              let rep = Jsonrpc.response_of_string content in
              rep
            else
              {success = true; contents = Null; notif = true})
