open Datatypes

let build_range l c1 c2 = { start = { line = l-1; character = c1 };
                            end_  = { line = l-1; character = c2 }; }

let basicDiag = {
  range = build_range 0 0 0;
  severity = None;
  code = None;
  source = None;
  message = "";
  relatedInformation = None }

let uri_to_path uri =
  let l = String.length uri in
  let res = String.sub uri 7 (l-7) in
  res

let path_to_uri path =
  "file://" ^ path

let pos_to_buffer_pos buf line char =
  let lines = String.split_on_char '\n' buf in
  let rec count_chars n ls cnt =
    if n = 0 then cnt
    else
      let l, ls = List.hd ls, List.tl ls in
      count_chars (n-1) ls (cnt + (String.length l) + 1) (* accounting for the newline char *)
  in
   let before = count_chars line lines 0 in
  before + char

let spacing_chars  = ['(';')';'\n';' '] (* to be completed *)
let operator_chars = ['+';'-';'.']

(* returns an offset *)
let find_ident_start buf pos_buf =
  let rec r offset =
    let c = String.get buf (pos_buf + offset) in
    if List.mem c spacing_chars || List.mem c operator_chars
    then offset+1
    else r (offset-1)
  in r 0

let pos_to_location pos =
  let (f, l, c1, c2) = Why3.Loc.get pos in
  { uri = path_to_uri f;
    range = { start = { line = l-1; character = c1 };
              end_  = { line = l-1; character = c2 }}}
