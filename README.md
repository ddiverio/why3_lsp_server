## why3_lsp_server README file

# Disclamer

At the moment, it's full of bugs, but it's yet to have less of
them. For example, it doesn't even shutdown properly. EDIT: Now it
does.

# Dependencies

- Ocaml,
- Dune,
- ocaml-rpc (the git version if you intend to use it with VSCode),
- Why3,
- For emacs : version 26.3 along with flymake.
- For VSCode : version 1.43.0.

# Installing dependencies

## General

Via git:

- `git clone https://github.com/mirage/ocaml-rpc`
- `cd ocaml-rpc`
- `opam install .` (* It should work. *)

Via opam:

`opam install rpclib rpclib-js ppx_deriving_rpc`

Currently not working with Visual Studio Code. Use the latest git
version. The opam version works with emacs though.

## VSCode-specific

Using npm, or any other package manager, you should install
vscode-languageclient.

# Building the project

Don't use Dune 2 yet, it doesn't work with ocaml-rpc's ppx.
- `dune build`
- `dune install`

## Emacs-specific

### Melpa configuration: installing lsp-mode

I use elpa to install lsp-mode in emacs :

`M-x package-install ESC lsp-mode`

If you run into trouble when installing lsp-mode, or if you didn't
configure elpa yet, you may consider looking at elpa's documentation,
at <https://elpa.gnu.org/>.

### Optional: flycheck/flymake

`M-x package-install ESC flycheck`

# lsp-why3 installation

## Emacs

Copy the lsp-why3.el file to ~/.emacs.d/lisp/ (assuming it's in your
.emacs path). It's better to have the why3.el file installed, from
the why3 repository.

Then, add the following lines to your .emacs file:


    (require 'lsp-mode) ;; If not already present
    (require 'lsp) ;; same
    (require 'why3) ;; Available in the why3 repository
    (require 'lsp-why3)
    (add-hook 'why3-mode-hook #'lsp)


## How to test why3_lsp_server with Emacs

- Add `(setq lsp-log-io t)` to your .emacs file.
- `M-x lsp-workspace-show-log`

It also logs stuff to ~/.why3lsplog. Indefinitely. Eating your disk
drive space little by little.

## VS Code

Install the why3-lsp extension from this repository.

<https://vscode-docs.readthedocs.io/en/stable/extensions/install-extension/>

# Implemented commands

## Emacs

`M-x lsp-why3-prove` launches prove on the current file.

`(add-hook 'why3-mode-hook #'(lambda () (add-hook 'after-save-hook
#'lsp-why3-prove)))` to activate lsp-why3-prove on save.


## VSCode

Ctrl-Maj-P then search for 'prove'.

# why3_lsp_server per-project configuration

In order to configure more thoroughly the execution of the Why3
libraries associated to the language server, why3_lsp_server supports
Why3 configuration files. For example, if you want to add an extra
library load path, you just have to create a .why3_lsp_server.conf
file in the same folder as the file you want to prove with these
contents:

    [main]
    loadpath = .

