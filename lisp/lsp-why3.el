(require 'lsp-mode)
(require 'why3)

(defgroup lsp-why3 nil
  "Customization group for ‘lsp-why3’."
  :group 'lsp-mode)

(defcustom lsp-why3-process-path-server
  "why3_lsp_server"
  "The path for starting the why3_lsp_server."
  :group 'lsp-why3
  :type '(choice string))

(defun lsp--why3-server-command ()
  "Command and arguments for launching the inferior why3_lsp_server process."
   (list lsp-why3-process-path-server))

(add-to-list 'lsp-language-id-configuration '(why3-mode . "why3"))

(lsp-register-client
    (make-lsp-client :new-connection (lsp-stdio-connection "why3_lsp_server")
                     :major-modes '(why3-mode)
                     :server-id 'why3_lsp_server))

(defun lsp-why3-prove ()
  "Launches why3 prove on the LSP server"
  (interactive)
  (save-buffer)
  (lsp--cur-workspace-check)
  (lsp--send-execute-command
   "prove"
   `(:file ,buffer-file-name)))

(provide 'lsp-why3)
