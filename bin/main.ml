open Why3lsp

module M=Idl.IdM
module MyIdl=Idl.Make(M)
module Server=ServerAPI.ServerAPI(MyIdl.GenServer ())

let serve_requests rpcfn =
  while true do
    Transport.serve_next_request rpcfn |> M.run
  done

let register_rpc_functions () =
  Server.initialize Engine.initialize;
  Server.initialized_n (MyIdl.ErrM.return ());
  Server.didOpen Engine.didOpen;
  Server.didClose Engine.didClose;
  Server.didChangeConfiguration Engine.didChangeConfiguration;
  Server.didSave Engine.didSave;
  Server.declaration Engine.declaration;
  Server.executeCommand Engine.executeCommand;
  Server.shutdown (Engine.shutdown ());
  Server.exit_n Engine.exit_n

let call_of_string str =
  let _, _, call = Jsonrpc.version_id_and_call_of_string_option str in call

let start_server () =
  Logger.log_to_file (Format.sprintf "Server loaded on %s." (Logger.string_date ()));
  register_rpc_functions ();
  (* setup the pipes *)
  let rpc_fn = MyIdl.server Server.implementation in
  let process x = rpc_fn (call_of_string x) in
  serve_requests process

let () = Why3.Debug.set_flag Why3.Glob.flag

let _ = start_server ()
